function subext --description 'Substitutes given file extensions for EXT: subext <EXT> <FILENAME>...'
    string replace -r '\\.\\w+$' $argv
end
