function myip4 --wraps=dig\ +short\ A\ \ \ \ myip.opendns.com\ \'@resolver1.opendns.com\' --description alias\ myip4=dig\ +short\ A\ \ \ \ myip.opendns.com\ \'@resolver1.opendns.com\'
  dig +short A    myip.opendns.com '@resolver1.opendns.com' $argv
        
end
