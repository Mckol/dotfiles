function lssh --wraps='ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no' --description 'alias lssh=ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no'
  ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no $argv
        
end
