function lscp --wraps='scp -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no' --description 'alias lscp=scp -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no'
  scp -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no $argv
        
end
