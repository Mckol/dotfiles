if status is-login && test -e /etc/profile
    bass source /etc/profile
end

if status is-login && test -e ~/.profile
    bass source ~/.profile
end

fish_add_path -a ~/.local/bin/

set -gx EDITOR nvim
set -gx VISUAL nvim

set -gx LESS "--quit-if-one-screen --RAW-CONTROL-CHARS --mouse"

set -gx LANG "en_US.UTF-8"
set -gx LANGUAGE "en_US.UTF-8"
set -gx LC_CTYPE "en_US.UTF-8"
set -gx LC_NUMERIC "en_US.UTF-8"
set -gx LC_TIME "en_GB.UTF-8"
set -gx LC_MONETARY "pl_PL.UTF-8"
set -gx LC_NAME "pl_PL.UTF-8"
set -gx LC_ADDRESS "pl_PL.UTF-8"
set -gx LC_TELEPHONE "pl_PL.UTF-8"
set -gx LC_MEASUREMENT "pl_PL.UTF-8"
set -gx LC_IDENTIFICATION "pl_PL.UTF-8"
