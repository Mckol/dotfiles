# Compatible with fish versions that do not support set -f (<3.4)

# Commands to run in interactive sessions can go here
if status is-interactive
    # Prompt setup
    set -g fish_color_user green
    set -g fish_color_host bryellow
    set -g fish_color_host_remote yellow
    set -g fish_color_cwd brpurple
    set -ge fish_color_cwd_root

    fish_vi_key_bindings

    # An easily accessible keybind to accept autosuggestions
    bind \ef accept-autosuggestion
    bind --mode insert \ef accept-autosuggestion
    bind \eF forward-word
    bind --mode insert \eF forward-word

    # Add visual_line mode
    bind --preset -M visual | sed -e 's/-M visual/-M visual_line/' | source -
    bind -m visual_line V beginning-of-line begin-selection end-of-line repaint-mode
    bind -M visual_line j down-line end-of-line swap-selection-start-stop beginning-of-line swap-selection-start-stop
    bind -M visual_line k up-line beginning-of-line swap-selection-start-stop end-of-line swap-selection-start-stop
    bind -M visual_line -m default \e end-selection repaint-mode
end

function fish_mode_prompt --description "disable default vi prompt"
end
function fish_prompt --description 'My custom prompt'
    set -l last_pipestatus $pipestatus
    set -lx __fish_last_status $status # Export for __fish_print_pipestatus.
    set -l normal (set_color normal)

    set -q fish_color_status
    or set -g fish_color_status red
    set -q fish_color_suffix
    or set -g fish_color_suffix brblue
    set -q fish_color_suffix_root
    or set -g fish_color_suffix_root brred

    set -l mode_prompt
    if test "$fish_key_bindings" = fish_vi_key_bindings
        or test "$fish_key_bindings" = fish_hybrid_key_bindings
        switch $fish_bind_mode
            case default
                set mode_prompt (set_color --bold brgreen)'<'
            case insert
                set mode_prompt (set_color --bold brblue)'>'
            case replace_one
                set mode_prompt (set_color --bold cyan)'R'
            case replace
                set mode_prompt (set_color --bold magenta)'R'
            case visual
                set mode_prompt (set_color --bold cyan)'V'
            case visual_line
                set mode_prompt (set_color --bold magenta)'V'
        end
        set mode_prompt $mode_prompt(set_color normal)
    end

    # Color the prompt differently when we're root
    set -l color_cwd $fish_color_cwd
    set -l suffix (set_color $fish_color_suffix)'>'
    if functions -q fish_is_root_user; and fish_is_root_user
        if set -q fish_color_cwd_root
            set color_cwd $fish_color_cwd_root
        end
        set suffix (set_color $fish_color_suffix_root)'#'
    end

    set -l suffix
    if set -q mode_prompt
        set suffix $mode_prompt
    end

    # Write pipestatus
    # If the status was carried over (if no command is issued or if `set` leaves the status untouched), don't bold it.
    set -l bold_flag --bold
    set -q __fish_prompt_status_generation
    or set -g __fish_prompt_status_generation $status_generation
    if test $__fish_prompt_status_generation = $status_generation
        set bold_flag
    end
    set __fish_prompt_status_generation $status_generation

    set -l status_color (set_color $fish_color_status)
    set -l statusb_color (set_color $bold_flag $fish_color_status)
    set -l prompt_status (__fish_print_pipestatus "[" "]" "|" "$status_color" "$statusb_color" $last_pipestatus)

    set -l login (prompt_login)':'
    #if test (tput cols) -lt 90
    #    set login
    #end

    set -l vcs (fish_vcs_prompt)$normal
    if test (tput cols) -lt 120
        set vcs
    end

    #echo -n -s (prompt_login)' ' (set_color $color_cwd) (prompt_pwd) $normal (fish_vcs_prompt) $normal " "$prompt_status $suffix " "
    echo -n -s [ $login (set_color $color_cwd) (prompt_pwd) $normal $vcs ] " "$prompt_status $suffix $normal " "
end
function fish_right_prompt
    set -l normal (set_color normal)

    echo -n -s [ (set_color brblue) (date +%H:%M) $normal ]
end

set -g fish_greeting

# Environment variables
set -gx EDITOR vim
set -gx VISUAL vim

set -gx LESS "--quit-if-one-screen --RAW-CONTROL-CHARS --mouse"

set -gx LANG "en_US.UTF-8"
set -gx LANGUAGE "en_US.UTF-8"
set -gx LC_CTYPE "en_US.UTF-8"
set -gx LC_NUMERIC "en_US.UTF-8"
set -gx LC_TIME "en_GB.UTF-8"
set -gx LC_MONETARY "pl_PL.UTF-8"
set -gx LC_NAME "pl_PL.UTF-8"
set -gx LC_ADDRESS "pl_PL.UTF-8"
set -gx LC_TELEPHONE "pl_PL.UTF-8"
set -gx LC_MEASUREMENT "pl_PL.UTF-8"
set -gx LC_IDENTIFICATION "pl_PL.UTF-8"
