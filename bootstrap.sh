#!/bin/bash
set -eo pipefail

arch="$(uname --machine)"
case "$arch" in
    ('x86_64' | 'aarch64') echo "Supported system architecture: ${arch}";;
    (*) echo "Unsupported system architecture (${arch}). Exiting..."; exit 1;;
esac

case "$arch" in
    ('x86_64')
        declare -g fish_url='https://github.com/fish-shell/fish-shell/releases/download/4.0.0/fish-static-amd64-4.0.0.tar.xz';
        declare -g fish_sha256='e7bc962652763e9ca396fd81c1a6cc6105c78f4a82ec35b6bf5c3edacfbd2114';
        declare -g helix_url='https://github.com/helix-editor/helix/releases/download/25.01.1/helix-25.01.1-x86_64-linux.tar.xz';
        declare -g helix_sha256='f955966e01e46076ec617551a11f7914df8935068a1c0e27501d82e09e603c96';
    ;;
    ('aarch64')
        declare -g fish_url='https://github.com/fish-shell/fish-shell/releases/download/4.0.0/fish-static-aarch64-4.0.0.tar.xz';
        declare -g fish_sha256='401fc53d2a8d37f5cdf264c08e397f45f61b397454f2ac433ccfcb8085c62212';
        declare -g helix_url='https://github.com/helix-editor/helix/releases/download/25.01.1/helix-25.01.1-aarch64-linux.tar.xz';
        declare -g helix_sha256='9101eef949929399ce8d3d13827aa0ed91a49f814c734c7303962edc7cd6fc45';
    ;;
esac

tempdir="$(mktemp -d)"
echo "Created temporary directory at: ${tempdir}"
echo ''

cd "$tempdir"

echo 'Downloading fish...'
curl --fail --location --output 'fish.tar.xz' "$fish_url"
echo ''
echo "$fish_sha256  fish.tar.xz" > 'fish.tar.xz.sha256sum'
sha256sum --check 'fish.tar.xz.sha256sum'
echo ''

echo 'Downloading helix...'
curl --fail --location --output 'helix.tar.xz' "$helix_url"
echo ''
echo "$helix_sha256  helix.tar.xz" > 'helix.tar.xz.sha256sum'
sha256sum --check 'helix.tar.xz.sha256sum'
echo ''

echo 'Installing fish...'
mkdir -p ~/.local/bin
tar -xJf 'fish.tar.xz' -C ~/.local/bin
echo 'Done.'
echo ''

echo 'Installing helix...'
mkdir helix
tar -xJf 'helix.tar.xz' -C 'helix'
mkdir -p ~/.config/helix ~/.config/fish/completions
mv helix/*/runtime ~/.config/helix/
mv helix/*/contrib/completion/hx.fish ~/.config/fish/completions/
install -Dm 755 helix/*/hx ~/.local/bin/
echo 'Done.'
echo ''

# shellcheck disable=SC2016
echo 'Injecting "~/.local/bin" into $PATH in the ".bashrc"'
cat << EOF >> ~/.bashrc

# Added by dotfiles bootstrap script
export PATH="$HOME/.local/bin:${PATH}"
EOF
echo ''

echo 'Downloading fish config...'
curl --fail --output 'config.fish' 'https://gitlab.com/LunarEclipse363/dotfiles/-/raw/8d7dae25c3b95ab7f260fa31e31bc15554cd4b69/user_configs/fish/config_minimal.fish'
echo ''
echo '938ddbb24d5d47236838524aab7bead3999a9d2f3e9bb430ffbddb2033d4bc96  config.fish' > 'config.fish.sha256sum'
sha256sum --check 'config.fish.sha256sum'
echo ''

echo 'Downloading helix configs...'
curl --fail --output 'config.toml' 'https://gitlab.com/LunarEclipse363/dotfiles/-/raw/8d7dae25c3b95ab7f260fa31e31bc15554cd4b69/user_configs/helix/config.toml'
curl --fail --output 'languages.toml' 'https://gitlab.com/LunarEclipse363/dotfiles/-/raw/aeed8940354f221fb4d6140d1a2a23f784b1dbb3/user_configs/helix/languages.toml'
echo ''
echo 'f928b8f0eea0bd4fb6d58f9ccbf612c22a985bb453996fb3ba9ddfe14b99bbc6  config.toml' > 'helix-configs.sha256sums'
echo '32cb8b62f010bd0ab09ccc8f8ddae9ccf7261ba87f0a908188450f1f74835b25  languages.toml' >> 'helix-configs.sha256sums'
sha256sum --check 'helix-configs.sha256sums'
echo ''

echo 'Installing fish config...'
install -Dm 644 'config.fish' ~/.config/fish/
echo ''

echo 'Installing helix configs...'
install -Dm 644 'config.toml' 'languages.toml' ~/.config/helix/
echo ''

echo 'Cleaning up...'
cd - >/dev/null 2>&1 
rm -rf "$tempdir"
echo ''

echo 'Testing fish configuration...'
# shellcheck disable=SC1090
source ~/.bashrc
fish --install -c 'echo "Success!"' <<< 'yes'
echo ''

echo 'Adding fish auto-start to ".bashrc"...'
cat << EOF >> ~/.bashrc
if [[ $(ps --no-header --pid=$PPID --format=comm) != "fish" && -z ${BASH_EXECUTION_STRING} && ${SHLVL} == 1 && $(which fish) ]]
then
    shopt -q login_shell && LOGIN_OPTION='--login' || LOGIN_OPTION=''
    exec fish $LOGIN_OPTION
fi
EOF
echo 'Done.'
echo ''

echo 'Successfully bootstrapped configuration!'
