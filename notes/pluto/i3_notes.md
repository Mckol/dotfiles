# Environment variables
- `.profile`
- `exec --no-startup-id` export in i3 config

# Qt/GTK styles

## Packages
- theme packages `breeze breeze-gtk papirus-icon-theme`
- configuration packages `lxappearance qt5ct`

## Setup
1. set up GTK stuff in lxappearance
2. set up QT  stuff in qt5ct
3. set `QT_QPA_PLATFORMTHEME=qt5ct`
    - you can add `export QT_QPA_PLATFORMTHEME=qt5ct` to `~/.profile`


# Tmux 256 colors
- [`~/.tmux.conf` config file](https://unix.stackexchange.com/questions/1045/getting-256-colors-to-work-in-tmux/1049#1049)
- alias `tmux` to `tmux -2` 

# AUR
`yay`: `git clone https://aur.archlinux.org/yay.git && cd yay && makepkg -si && cd .. && rm -rf yay`

# Fonts
- `noto-fonts-all ttf-fantasque-sans-mono ttf-symbola ttf-liberation ttf-bitstream-vera`
- [a dive into fontconfig config](https://eev.ee/blog/2015/05/20/i-stared-into-the-fontconfig-and-the-fontconfig-stared-back-at-me/)
- working emojis `fontconfig-emoji-git`, more reading in the [upstream repo](https://github.com/stove-panini/fontconfig-emoji)

# Audio
- just pulseaudio should work
- pulseaudio is executed by systemd, adding it as an exec breaks it partially 
- cadence for jack + pulseaudio setup is also an option

# SysRq
`sudo bash -c "echo kernel.sysrq = 1 > /etc/sysctl.d/99-sysctl.conf"`

# Vim
- [vim-plug](https://github.com/junegunn/vim-plug)

## YouCompleteMe setup
- [installation guide](https://github.com/ycm-core/YouCompleteMe#linux-64-bit)

# Thinkpad related configuration

## Fingerprint reader
- `fprintd` [archwiki](https://wiki.archlinux.org/index.php/Fprint)
- can only have a single fingerprint per user, otherwise won't work at all
- need to configure pam
- anyone can add fingerprints without authentication by default

# i3 configuration / related stuff

## Status bar
`i3status-rust-git` [setup guide](https://github.com/greshake/i3status-rust)

## Lockscreen
`i3lock-color` refer to manpage

## Launcher
`rofi`

## Notifications
`dunst` [webpage](https://dunst-project.org/)

## Execs
- use `--no-startup-id` for programs that don't open a window

## Screenshot manager
~~`spectacle`~~
`flameshot`

## Terminal emulator
- `konsole`
- you can disable showing menubar by default in Settings > Configure Konsole > General

## File manager
- gui `dolphin`
- cli `mc`

## Multimedia keys etc.
- [bindings for i3](https://faq.i3wm.org/question/3747/enabling-multimedia-keys/?answer=3759#post-id-3759)

## Clipboard manager
- rofi-greenclip

# Display manager
- `lightdm-gtk-greeter lightdm-gtk-greeter-settings`

# Wallpaper
- `feh imagemagick`
- set wallpaper with `feh --bg-scale <path_to_image>`
- add `exec_always ~/.fehbg &` to i3 config file

# SSD
- [Enable TRIM](https://wiki.archlinux.org/index.php/Solid_state_drive#TRIM)

# Packages

## Graphics
- `krita gimp inkscape darktable imagemagick`
- `gwenview digikam`

## Dynamic swap & zram
`systemd-swap` [archwiki](https://wiki.archlinux.org/index.php/Swap#systemd-swap)

## Automounting usb drives
`udevil`

## Video recording & editing
`kdenlive obs-studio ffmpeg`

## Audio stuff
`audacity`

## Partitioning
- gui `gparted`
- cli `cfdisk`

## Data recovery
`testdisk`

## S.M.A.R.T.
- `smartmontools`
- [archwiki](https://wiki.archlinux.org/index.php/S.M.A.R.T.)
- additional configuration needed for notifications about issues

## Basic stuff
`zsh tmux vim git wget`

## Wacom tablet
- `xf86-input-wacom` xorg driver
- [configuration](https://wiki.archlinux.org/index.php/Wacom_tablet)

## Networking
- `wifi-menu dialog wpa_supplicant networkmanager`

## Monitor setup
- `arandr` gui tool

## Gaming

### Steam
- need to enable multilib repo
- `steam`
- [archwiki](https://wiki.archlinux.org/index.php/Steam)

#### Dying light
- workaround needed TODO

### Lutris
- `lutris`

## Office
`libreoffice-fresh`

## PDF
`firefox` can open that

## Web browser

### CLI
- `lynx` simplicity.
- `elinks` works with more websites than `lynx`

### GUI
- `firefox` main browser
- `chromium` with streamkeys for music on youtube

## File manager
- `nemo` + extensions
