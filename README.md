# Configs and notes

Some config files for my computers

See [ProtoFox](https://gitlab.com/LunarEclipse363/protofox) for my Firefox `userChrome.css`

## Organization
This repo is divided into 3 sections:
- `user_configs` - config files for desktop-independent software
- `xorg_configs` - config files specific to my [AwesomeWM](https://github.com/awesomeWM/awesome) setup
- `wayland_configs` - config files specific to my [niri](https://github.com/YaLTeR/niri) setup

## Screenshots
### AwesomeWM
![screenshot of my awesomewm setup](Xorg-AwesomeWM.webp)

### Niri
![screenshot of my niri setup](Wayland-niri.webp)

## TODO
- [ ] Any reasonable way to import these more easily.
    - [x] Machine-readable "what goes where" mapping (`map.json`)
    - [ ] Script for automatically extracting selected configs
- [x] Jupiter
    - [x] Basic Configs from Jupiter
    - [x] AwesomeWM-related Configs from Jupiter
    - [ ] General Arch-related Configs from Jupiter
    - [ ] Add Firefox setup notes
- [ ] Any useful general snippets for NixOS from Pluto (full config private)
